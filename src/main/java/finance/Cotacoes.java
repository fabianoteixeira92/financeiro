package finance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import core.Constantes;
import core.Staff;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class Cotacoes {

    public static void main(String[] args) throws IOException {

        try (InputStream is = new URL(Constantes.URL_COTACOES).openStream();
             Reader reader = new InputStreamReader(is, StandardCharsets.UTF_8)) {

            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            Staff staff = gson.fromJson(reader, Staff.class);

            // print staff
            String json = gson.toJson(staff);
            //System.out.println(staff.getResults().currencies.USD.name);

            System.out.println("Atualização da cotação das principais moedas para que você fique informado e tome as melhores decisões: " +
                    "\n" + staff.getResults().currencies.USD.name + " hoje está R$" + staff.getResults().currencies.USD.buy +
                    "\n" + staff.getResults().currencies.EUR.name + " hoje está R$" + staff.getResults().currencies.EUR.buy +
                    "\n" + staff.getResults().currencies.CAD.name + " hoje está R$" + staff.getResults().currencies.CAD.buy +
                    "\n" + staff.getResults().currencies.AUD.name + " hoje está R$" + staff.getResults().currencies.AUD.buy +
                    "\n" + staff.getResults().currencies.ARS.name + " hoje está R$" + staff.getResults().currencies.ARS.buy +
                    "\n" + staff.getResults().currencies.CNY.name + " hoje está R$" + staff.getResults().currencies.CNY.buy +
                    "\n" + staff.getResults().currencies.GBP.name + " hoje está R$" + staff.getResults().currencies.GBP.buy +
                    "\n" + staff.getResults().currencies.JPY.name + " hoje está R$" + staff.getResults().currencies.JPY.buy +
                    "\n" + staff.getResults().currencies.BTC.name + " hoje está R$" + staff.getResults().currencies.BTC.buy +
                    "");

        } catch (IOException e) {
                e.printStackTrace();
            }

        }


}
