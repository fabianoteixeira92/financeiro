package core.moeda;

public class USD{
    public String name;
    public double buy;
    public double sell;
    public double variation;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getBuy() {
        return buy;
    }

    public void setBuy(double buy) {
        this.buy = buy;
    }

    public double getSell() {
        return sell;
    }

    public void setSell(double sell) {
        this.sell = sell;
    }

    public double getVariation() {
        return variation;
    }

    public void setVariation(double variation) {
        this.variation = variation;
    }

    @Override
    public String toString() {
        return "USD{" +
                "name='" + name + '\'' +
                ", buy=" + buy +
                ", sell=" + sell +
                ", variation=" + variation +
                '}';
    }
}
