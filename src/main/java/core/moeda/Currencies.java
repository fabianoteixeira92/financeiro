package core.moeda;

public class Currencies{
    public String source;
    public USD USD;
    public EUR EUR;
    public GBP GBP;
    public ARS ARS;
    public CAD CAD;
    public AUD AUD;
    public JPY JPY;
    public CNY CNY;
    public BTC BTC;

    @Override
    public String toString() {
        return "Currencies{" +
                "source='" + source + '\'' +
                ", USD=" + USD +
                ", EUR=" + EUR +
                ", GBP=" + GBP +
                ", ARS=" + ARS +
                ", CAD=" + CAD +
                ", AUD=" + AUD +
                ", JPY=" + JPY +
                ", CNY=" + CNY +
                ", BTC=" + BTC +
                '}';
    }
}