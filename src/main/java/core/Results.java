package core;

import core.bitcoin.Bitcoin;
import core.moeda.Currencies;
import core.stocks.Stocks;
import core.tax.Tax;

import java.util.List;

public class Results{
    public Currencies currencies;
    public Stocks stocks;
    public List<String> available_sources;
    public Bitcoin bitcoin;
    public List<Tax> taxes;

    @Override
    public String toString() {
        return "Results{" +
                "currencies=" + currencies +
                ", stocks=" + stocks +
                ", available_sources=" + available_sources +
                ", bitcoin=" + bitcoin +
                ", taxes=" + taxes +
                '}';
    }
}
