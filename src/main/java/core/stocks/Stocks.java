package core.stocks;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Stocks{
    @JsonProperty("IBOVESPA")
    public IBOVESPA iBOVESPA;
    @JsonProperty("IFIX")
    public IFIX iFIX;
    @JsonProperty("NASDAQ")
    public NASDAQ nASDAQ;
    @JsonProperty("DOWJONES")
    public DOWJONES dOWJONES;
    @JsonProperty("CAC")
    public CAC cAC;
    @JsonProperty("NIKKEI")
    public NIKKEI nIKKEI;
}
