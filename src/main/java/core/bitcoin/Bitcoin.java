package core.bitcoin;

public class Bitcoin{
    public BlockchainInfo blockchain_info;
    public Coinbase coinbase;
    public Bitstamp bitstamp;
    public Foxbit foxbit;
    public Mercadobitcoin mercadobitcoin;
}