package core.bitcoin;

import java.util.List;

public class Coinbase{
    public String name;
    public List<String> format;
    public double last;
    public double variation;
}
