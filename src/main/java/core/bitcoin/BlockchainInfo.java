package core.bitcoin;

import java.util.List;

public class BlockchainInfo{
    public String name;
    public List<String> format;
    public double last;
    public double buy;
    public double sell;
    public double variation;
}
